<?php
// no direct access
defined('ABSPATH') or die();

if(!class_exists('LSD_Addons_Divi')):

/**
 * Listdom Addon Divi Class.
 *
 * @class LSD_Addons_Divi
 * @version	1.0.0
 */
class LSD_Addons_Divi extends LSD_Addons
{
    /**
	 * Constructor method
	 */
	public function __construct()
    {
        parent::__construct();
	}
    
    public function init()
    {
    }
}

endif;