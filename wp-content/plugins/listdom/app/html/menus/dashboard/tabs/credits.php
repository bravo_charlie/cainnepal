<?php
// no direct access
defined('ABSPATH') or die();
?>
<div class="lsd-credits-wrap lsd-mt-5">
    <p><?php echo sprintf(esc_html__("Listdom is developed by %s Totalery is a modern web development and design company that focused on WordPress market. We develop modern and clean WordPress Plugins for different purposes. Totalery started its business since 2013 and till now we had thousands of satisfied clients.", 'listdom'), '<strong><a href="https://totalery.com">Totalery Inc.</a></strong>'); ?></p>
    <h2><a href="https://totalery.com">Powered by Totalery!</a></h2>
</div>