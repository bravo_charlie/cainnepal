=== Listdom - Advanced Directory and Listing Plugin ===
Contributors: totalery
Donate link: https://totalery.com/
Tags: Business Directory, Listings, Classifieds, Directory Plugin, Store Locator
Requires at least: 4.0.0
Requires PHP: 5.6
Tested up to: 5.7.2
Stable tag: 2.1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Listdom is the king of WordPress directory and listing plugins that allows you to create an advanced classifieds website to showcase the listings in over 80 different skins and styles.

== Description ==

[youtube https://www.youtube.com/watch?v=OGVQGCUOaZ0]

Listdom is a powerful WordPress directory and listing plugin which is a free tool to create listing, directory and classifieds websites with modern and simple tools. Listdom has more than 80 different skins and views which are responsive, mobile-friendly, and free. Also it has so many features which cannot find all of them in a free plugin and one package. Using Listdom listing plugin is as simple as WordPress and it is following all standards that this CMS has, On the other hand it is fully compatible with other plugins that follow WordPress rules.

*   [Demo](https://totalery.com/listdom/)
*   [Documentation](https://totalery.com/listdom/documentation/)
*   [Support](https://totalery.com/support/)
*   [Features](https://totalery.com/listdom/features/)
*   [Live Chat](https://totalery.com/)

**WordPress Listing Plugin**

[Listdom](https://totalery.com/listdom/) has been developed according to latest best practices, design patterns, trending design methods and technologies. We are offering all the advanced [features](https://totalery.com/listdom/features/) that a directory and listing plugin need in [Listdom Pro](https://totalery.com/listdom/), our pro and lite version has so many features which you can use to create listing, directory and classifieds website as you want.
Listdom Lite offers you everything that you need to create amazing listings and show them in more than 80 different ways.

As you know, WordPress does not have any built-in feature that you create a listing and directory website, so Listdom is here to help you to create a website as you wish. Listdom is an amazing plugin which allows you to create your website so simple and it is compatible with WordPress and all plugins that follow WordPress standards. Listdom is compatible with all themes and you do not need to have extra CSS for that.

**Amazing Documentation and Support**

Listdom has an amazing documentation which explains everything about this powerful WordPress listing plugin. We are explaining the features step by step and you can see everything here.
Also we have Support and Live Chat which you can contact us and we will address everything as soon as possible.

Demo Link: [https://totalery.com/listdom/](https://totalery.com/listdom/)
Support: [https://totalery.com/support/](https://totalery.com/support/)
Live Chat: [https://totalery.com/](https://totalery.com/)

Here you can have a look on some of [Listdom features](https://totalery.com/listdom/features/)!

**Developer Features**

*   Full developers friendly
*   Support changing archive page and single page structure in the active them
*   100% use of WordPress structure
*   Override the skins and styles in the active theme
*   Define different filters for shortcodes to show different views
*   Ability to fire custom functions using WordPress actions API

**Key Features**

*   Different skins including [list](https://totalery.com/listdom/demo/list-view/style-1-listing-first/), [grid](https://totalery.com/listdom/demo/grid-view/style-1-listing-first/), [masonry](https://totalery.com/listdom/demo/masonry-view/style-1-categories/), [single map](https://totalery.com/listdom/demo/single-map/default/), [halfmap](https://totalery.com/listdom/demo/half-map-split-view/style-1-right-map/), [carousel](https://totalery.com/listdom/demo/carousel/style-1/), [slider](https://totalery.com/listdom/demo/slider/style-1/) and many more!
*   [Listing Frontend Submission](https://totalery.com/listdom/demo/frontend-listing-manager/frontend-dashboard/) [PRO]
*   Listing Owner Module
*   Contact Owner
*   Social Share Module
*   [Advanced Price Module](https://totalery.com/listdom/documentation/setting-up-listing/price-options/)
*   [Image Gallery Module](https://totalery.com/listdom/documentation/setting-up-listing/gallery/)
*   [Listing Contact Information](https://totalery.com/listdom/documentation/setting-up-listing/contact-details/)
*   [Advanced Availability Time Module](https://totalery.com/listdom/documentation/setting-up-listing/work-hours/)
*   [Nice Remark Module](https://totalery.com/listdom/documentation/setting-up-listing/remark/)
*   [Custom Fields](https://totalery.com/listdom/documentation/setup-features/attributes/) [PRO]
*   [Location Tools](https://totalery.com/listdom/documentation/setup-features/locations/)
*   [Powerful Shortcodes Generator](https://totalery.com/listdom/documentation/setting-up-shortcodes/make-advanced-shortcodes/)
*   [Advanced Map Search Feature](https://totalery.com/listdom/demo/grid-view/style-1-map-first/) [PRO]
*   [Draw Tools on Map for Search](https://totalery.com/listdom/demo/list-view/style-1-map-first/) [PRO]
*   [Map GPS Search](https://totalery.com/listdom/demo/listgrid-view/style-1-map-first/) [PRO]
*   Sort Module on All Applicable Skins
*   Ability to Change Default Sort Parameters
*   [Easy Auto Updates](https://totalery.com/listdom/documentation/installation/how-to-auto-update-plugin/)
*   [Advanced Configuration Options](https://totalery.com/listdom/documentation/settings/general/)
*   [Google Recaptcha](https://totalery.com/listdom/documentation/settings/general/)
*   Advanced [Label](https://totalery.com/listdom/documentation/setup-features/labels/) and [Tagging](https://totalery.com/listdom/documentation/setup-features/tags/) System for Listings
*   [Google Maps](https://totalery.com/listdom/demo/single-map/facebook/)
*   [OpenStreetMap](https://totalery.com/listdom/demo/openstreetmap-free/) [PRO]
*   Advanced Marker Clustering
*   Color Manager
*   [Custom Styles](https://totalery.com/listdom/documentation/settings/custom-styles/)
*   [Friendly Slug Manager](https://totalery.com/listdom/documentation/settings/slugs/)
*   [Ability to Override Archive Page](https://totalery.com/listdom/documentation/settings/archive-pages/)
*   [Ability to Change Style and Modules per Listing](https://totalery.com/listdom/documentation/settings/details-page/) [PRO]
*   Translation Ready
*   Multilingual Ready
*   Page Builders Compatibility
*   Cache Plugins Compatibility
*   Multi-Site Compatibility
*   SEO Plugins Compatibility
*   Block Editor Compatibility
*   WPML Compatible
*   [Claim Feature](https://totalery.com/listdom/documentation/addons/claim-addon/) [ADDON]
*   [Topup Listings](https://totalery.com/listdom/documentation/addons/top-up-addon/) [ADDON]
*   [Bookmarks Listings](https://totalery.com/listdom/documentation/addons/favorite-addon/) [ADDON]
*   [CSV Importer](https://totalery.com/listdom/documentation/addons/csv-importer/) [ADDON]
*   [Android / iOS Mobile Application](https://totalery.com/listdom/documentation/addons/android-ios-app-addon/) [ADDON]
*   [Rank Addon](https://totalery.com/listdom/documentation/addons/rank-addon/) [ADDON]
*   [Rate and Reviews](https://totalery.com/listdom/documentation/addons/reviews-addon/) [ADDON]
*   [Subscriptions](https://totalery.com/listdom/documentation/addons/subscriptions-addon/) [ADDON]
*   [Labelize](https://totalery.com/listdom/documentation/addons/labelize-addon/) [ADDON]
*   [Advanced Map](https://totalery.com/listdom/documentation/addons/advanced-map/) [ADDON]

**Display Features**

*	[Half Map / Split View](https://totalery.com/listdom/demo/half-map-split-view/)
*	[List + Grid View](https://totalery.com/listdom/demo/listgrid-view/)
*   [List Views](https://totalery.com/listdom/demo/list-view/)
*   [Grid Views](https://totalery.com/listdom/demo/grid-view/)
*   [Table views](https://totalery.com/listdom/demo/table-view/)
*   [Masonry views](https://totalery.com/listdom/demo/masonry-view/)
*   [Archive Pages](https://totalery.com/listdom/demo/archive-pages/)
*   Font Awesome icons
*   [Define colors for the skins as you wish](https://totalery.com/listdom/documentation/settings/general/)
*   [Use predefined colors](https://totalery.com/listdom/documentation/settings/general/)
*   [Map views](https://totalery.com/listdom/demo/single-map/)
*   [Mixed views](https://totalery.com/listdom/demo/multiple-shortcodes/)
*   [Cover views](https://totalery.com/listdom/demo/covre-view/)
*   Support different skins for views
*   [Carousel Views](https://totalery.com/listdom/demo/carousel/)
*   [Slider views](https://totalery.com/listdom/demo/slider/)
*   Show listings on the [Google Maps](https://totalery.com/listdom/demo/half-map-split-view/style-2-right-map/) based on their location
*   Custom color for settings
*   Category filter option
*   Location filter option
*   Tags filter option
*   Features filter option
*   Author filter options
*   [Search option](https://totalery.com/listdom/demo/search/)

**Upcoming Features!**

*   Listing Team
*   Currency Management
*   And many more ...

Do you have a right to left website?! Do not worry! Listdom is RTL ready!

== Installation ==

For installing Listdom please follow below steps:

1. Install it either via the WordPress.org plugin directory, or by uploading the files to your server.
2. Activate the plugin through the 'Plugins' menu in WordPress.

== Frequently Asked Questions ==

= Does Listdom have a feature to show listings on a map? =

Absolutely! Listdom supports Google Maps! Also Listdom PRO includes OpenStreetMap too so you can use your desired map provider.

= Does Listdom support custom fields? =

Yes, of course! You're able to create your desired custom fields using Attributes in Listdom Pro. Even you're able to create category specific fields!

= Is it possible to manage Listing details page modules? =

Yes, using our advanced configuration options you're able to disable / enable the modules on listing details page and also you're able to change their positions if you're using Style 1.

= Do I have to use Google Maps? it's a little expensive! =

Listdom PRO supports free OpenStreetMap! Also it's possible to use both of them at once! For example in details page you can use Google Maps but in List, Grid and other skins you can use OpenStreetMap.

= I like to use OpenStreetMap but with more beautiful tiles! What can I do? =

You're able to insert your mapbox API key in configuration options and use mapbox tiles instead of OpenStreetMap tiles.

== Screenshots ==

1. Listing Details (Style 2)
2. Listing Details (Style 1)
3. Halfmap Skin
4. Grid Skin
5. Masonry Skin
6. Table Skin
7. List Skin
8. Cover Skin
9. OpenStreetMap (Completely Free)
10. Add / Edit Listing Page
11. Powerful Shortcode Builder
12. Advanced Configuration Options

== Changelog ==

= 2.1.1 =
* Added Instagram to social network options.
* [PRO] Improved guest user listing submission.
* Improved the social network options.
* Fixed some PHP notices.
* Fixed an issue in status change notification.

= 2.1.0 =
* Added an option to change date format of date picker fields.
* Added an option to disable "Listing Link" field.
* Added HTML editor to the remark field.
* Added an ability to switch languages in REST API.
* Added some new endpoints to the REST API for multilingual websites.
* Fixed an issue regarding halfmap skin.
* Fixed an issue in search module.

= 2.0.0 =
* [ADDON] Released Franchise addon!
* [ADDON] Released Compare addon!
* [ADDON] Released APS addon!
* [ADDON] Released Stats addon!
* [PRO] Added an ability to disable image display per short-code.
* [PRO] Added a feature to change the listing link method with normal, blank, and disabled options.
* Added new notification for listing status update.
* Added report abuse element.
* Fixed some issues regarding schema feature.
* Fixed some issues.

= 1.9.0 =
* [ADDON] Released ACF Integration addon!
* [ADDON] Released Auction addon!
* [ADDON] Released BuddyPress Integration addon!
* [ADDON] Released KML addon!
* Added price class feature.
* Added an option to display human readable criteria to the search module.
* Added an option to change the currency position.
* Added style 3 to list, grid, listgrid, and halfmap skins.
* Added style 5 to carousel skin.
* Added style 4 to cover skin.
* Added style 3 to masonry skin.
* Added style 3 to table skin.
* Added website field to contact details of listing and owner.
* Disabled scroll wheel on leaflet map.

= 1.8.0 =
* [ADDON] Released Booking addon!
* [ADDON] Released Multiple Categories addon!
* [ADDON] Released Advanced Icon addon!
* [ADDON] Released Listing Visibility addon!
* [PRO] Added an option to load locations and features in multiple dropdown instead of checkboxes in frontend dashboard.
* [PRO] Added required option for the attribute fields.
* Added a new feature to select some predefined terms in taxonomy fields of search builder.
* Added list / grid switcher in the half map skin.
* Added random sort option.
* Added an option to manage zoom levels of GPS feature.
* Added no listing message.
* Improved the settings menu.
* Fixed an issue of not having HTML codes in Notifications.
* Fixed an issue in modal content.
* Fixed an issue in featured image uploading for guest users.

= 1.7.0 =
* [ADDON] Released Team addon!
* Added an option to load listing details page into light-box on click of marker.
* Improved the listdom icons.
* Fixed some issues.

= 1.6.3 =
* Fixed some UI issues related to icons.

= 1.6.2 =
* [ADDON] Released Rate & Review addon!
* [PRO] Added radius search in search module.
* Added compatibility with WP 2020 theme.
* Applied many improvements.
* Fixed an issue in assigning listing to user after approving by admin.
* Fixed an issue in showing map element in style 1.

= 1.6.1 =
* [ADDON] Released Labelize addon!
* [ADDON] Released Subscriptions addon!
* [ADDON] Released Paid Member Subscriptions Integration addon!
* [PRO] Added hierarchical dropdown method for taxonomies in search builder.
* Added some new endpoints to the API.
* Improved security of listdom and addons.
* Fixed an issue in search builder regarding default values.
* Fixed some issues.

= 1.6.0 =
* [ADDON] Released Claim addon!
* [ADDON] Released Topup addon!
* Added an ability to show all values of a certain attribute in the search builder.
* Improved security of listdom.
* Fixed an issue regarding showing all attributes in API.

= 1.5.0 =
* [ADDON] Released Favorites addon!
* [ADDON] Released Rank addon!
* [PRO] Added schema (Structured Data) feature to boost SEO.
* Improved Listdom Restful API.
* Fixed some issues in permission of Restful API.
* Fixed an issue in search of text fields.
* Fixed some issues.

= 1.4.0 =
* [ADDON] Released Mobile Application addon!
* Added Listdom Restful API.
* [PRO] Added embed code feature to submit videos, virtual tours etc. for certain listings!.
* Fixed category hierarchy issue on attributes menu.
* Fixed a conflict between Listdom and Elementor.
* Fixed some PHP notices.

= 1.3.1 =
* [ADDON] Released CSV addon!
* [PRO] Added dashboard module controls so the modules can be disabled / enabled.
* [PRO] Added ability to export and import listing gallery.
* Fixed an issue in target page of search form when the shortcode loads in archive instead of singular page.
* Fixed some PHP notices.

= 1.3.0 =
* [ADDON] Released Advanced Map addon!
* [PRO] Added advanced import and export system.
* Fixed an issue in map search feature.

= 1.2.1 =
* [PRO] Added hierarchical support for category taxonomy.
* Added an dashboard notification system to manage the system emails.
* Added HTML marker to leaflet.
* Improved leaflet clustering for polygon, rectangle and polylines.
* Fixed an issue in leaflet clustering.

= 1.2.0 =
* [PRO] Added dashboard shortcode to add and manage listings from frontend.
* [PRO] Added clustering feature for leaflet map.
* Added search functionality to the shortcode builder for different skins.
* Fixed some issues on search builder.
* Fixed an issue in availability form.
* Fixed an issue in warning of Google Maps API Key.
* Fixed an issue in saving the attributes.
* Fixed a query issue on skins.

= 1.1.1 =
* Added multiple dropdown search methods.
* Improved dummy data importer to import a default search form too.
* Fixed some issues on settings page.

= 1.1.0 =
* Added Advanced Search Builder.
* Added ability to hide email, fax, mobile, etc. in owner element.
* Improved search widget to work with search builder!
* Fixed some issues.

= 1.0.2 =
* [PRO] Improved design of GPS icon in the map module.
* Fixed some tiny issues.

= 1.0.1 =
* [PRO] Improved activation and update process!

= 1.0.0 =
* Initial version released with lots of features.